
import org.junit.Test;
import org.omg.CORBA.PUBLIC_MEMBER;

import cn.edu.zjut.vis.print.VPrintHelper;
import processing.core.PApplet;

import java.io.BufferedReader;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SpatialTest extends PApplet  {

    static List<Point> _pointList = null;

    private void LoadPointsFromFile(String source) {
        String[] item;
        String[] lines = readAllTextFileLines(source);
        
        int count = 0;
        
        for (String line : lines) {
            item = line.split("\t");
            _pointList.add(new Point(Double.parseDouble(item[5]), Double.parseDouble(item[4]), Double.parseDouble(item[4])));
            if(++ count  == 113){
            	break;
            }
        }
    }

    private static String[] readAllTextFileLines(String fileName) {
        StringBuilder sb = new StringBuilder();

        try {
            String textLine;

            BufferedReader br = new BufferedReader(new FileReader(fileName));

            while ((textLine = br.readLine()) != null) {
                sb.append(textLine);
                sb.append('\n');
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
            if (sb.length() == 0)
                sb.append("\n");
        }
        return sb.toString().split("\n");
    } 
    
    
    public void setup() {
        // original setup code here ...
        size(800, 800);
        
        background(255);
        
        // prevent thread from starving everything else
        noLoop();
        noFill();
    }

    
    public int cut = 4;
    
    public void drawrect(Node n, int depth){



    	if(depth > cut)
    		return;
    	
    	if(n == null){
    		return; 
    	}
    	//else if(n != null && n.getNw() == null && n.getNe() == null && n.getSe() == null && n.getSw() == null){
    	//else if(depth != 0 && (n.getNw() != null || n.getNe() != null || n.getSe() != null || n.getSw() != null)){	

            float x = (float) (180 + n.getX());
            float y = (float) (90 + n.getY());
    		stroke(0,0,0, 44);
    		rect(x, y, (float)n.getW(), (float)n.getH());	
    	//}
    	
    	depth ++;
		drawrect(n.getNw(), depth);
		drawrect(n.getNe(), depth);
		drawrect(n.getSe(), depth);
		drawrect(n.getSw(), depth);
    	
    	return;
    	
    }
    
    public void draw() {
    	
    	//translate(360,40 );
    	
    	
    	//scale(-1,1);

        _pointList = new ArrayList<Point>();
        //URL classpathResource = Thread.currentThread().getContextClassLoader().getResource("");
        //String resourcePath = classpathResource.getPath()+"points.txt";
        //LoadPointsFromFile("/Users/sungodoor/Documents/workspace/TwitterStreamFetch/points.txt");
        LoadPointsFromFile("/Users/sungodoor/Documents/workspace/TwitterStreamFetch/aaa.txt");
        
        
       // assertEquals("Expecting 844 points",844,_pointList.size());

       
        
        //http://spatialreference.org/ref/epsg/4326/
        QuadTree qt = new QuadTree(-180.000000, -90.000000, 180.000000, 90.000000);
        System.out.println(_pointList.size());
        for(Point pt:_pointList)
        {
            qt.set(pt.getX(), pt.getY(), pt.getValue());

           
            // point(x * 2, (400-y) * 2 - 300);
        }
        //Point[] points = qt.searchIntersect(-84.375,27.059,-78.75,31.952 );
        //System.out.print( Arrays.asList(points).toString());
        //assertEquals(60,points.length);
        
        
       
        
        for(Point pt:_pointList)
        {

            float x = (float) (180 + pt.getX());
            float y = (float) (90 + pt.getY());
            strokeWeight(1.0f);
            point(x, y);
            // point(x * 2, (400-y) * 2 - 300);
        } 
        strokeWeight(1.0f);
        noFill();
        drawrect(qt.getRootNode(), 0);
        
    }
 


}
