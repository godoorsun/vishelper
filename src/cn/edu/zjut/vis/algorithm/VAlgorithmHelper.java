package cn.edu.zjut.vis.algorithm;

import java.io.BufferedWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import org.junit.internal.builders.SuiteMethodBuilder;

import cn.edu.zjut.vis.file.VFileHelper;
import cn.edu.zjut.vis.print.VPrintHelper;
import cn.edu.zjut.vis.type.TwoTuple;

public class VAlgorithmHelper {

	
	
	
	public static int getListSum(ArrayList<Integer> l){
		int sum = 0;
		for(Integer a: l)
			sum+=a;
		return sum;
	}
	
	public static int getArraySum(Integer[] l){
		int sum = 0;
		for(Integer a: l)
			sum+=a;
		return sum;
	}

	
	public static TwoTuple<Integer, Double> computeCrossCorrelation(ArrayList<Double> x, ArrayList<Double> y, int maxdelay)
	{
		int n = x.size();
		
		   int i,j;
		   double mx,my,sx,sy,sxy,denom,r;

		   TwoTuple<Integer, Double> result = new TwoTuple<Integer, Double>();
		   result.second = Double.MIN_VALUE;
		   result.first = 0;
		   
		   /* Calculate the mean of the two series x[], y[] */
		   mx = 0;
		   my = 0;   
		   for (i=0;i<n;i++) {
		      mx += x.get(i);
		      my += y.get(i);
		   }
		   mx /= n;
		   my /= n;

		   /* Calculate the denominator */
		   sx = 0;
		   sy = 0;
		   for (i=0;i<n;i++) {
		      sx += (x.get(i) - mx) * (x.get(i)- mx);
		      sy += (y.get(i) - my) * (y.get(i) - my);
		   }
		   denom = Math.sqrt(sx*sy);
 
		   
		   /* Calculate the correlation series */
		   for (int delay=-maxdelay;delay<maxdelay;delay++) {
		      sxy = 0;
		      for (i=0;i<n;i++) {
		         j = i + delay;
		         if (j < 0 || j >= n)
		            continue;
		         else
		            sxy += (x.get(i)- mx) * (y.get(j) - my);
		         /* Or should it be (?)
		         if (j < 0 || j >= n)
		            sxy += (x[i] - mx) * (-my);
		         else
		            sxy += (x[i] - mx) * (y[j] - my);
		         */
		      }
		      r = sxy / denom;
		      if(r > result.second){
		    	  result.second = r;
		    	  result.first =  delay;
		      }
		      
		      //GuodaoToolbox.printlnInTabFormat(delay, r);
		      /* r is the correlation coefficient at "delay" */

		   }
		   
		   //GuodaoToolbox.printlnInTabFormat(result);
		   return result;
	}
	
	
	
	
	
	public static Map sortByValue(Map unsortedMap) {
		Map sortedMap = new TreeMap(new ValueComparator(unsortedMap));
		sortedMap.putAll(unsortedMap);
		return sortedMap;
	}

	public static void sortByValueAndSave(String path, Map unsortedMap){

		Map sortedMap = new TreeMap(new ValueComparator(unsortedMap));
		sortedMap.putAll(unsortedMap);
		
		try{
			BufferedWriter bw = VFileHelper.getBufferedWriter(path);
			TreeMap<String, Integer> sorted_map = (TreeMap<String, Integer>) sortByValue(unsortedMap);
			for(String term_name : sorted_map.keySet()){
				bw.write(term_name + "\t" + unsortedMap.get(term_name) + "\n");
			}
			bw.flush();
			bw.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public static Map sortByKey(Map unsortedMap) {
		Map sortedMap = new TreeMap();
		sortedMap.putAll(unsortedMap);
		return sortedMap;
	}
	
	
	public static double computeAverageValueofArray(double[] a){
		double sum = 0;
		for(double d : a)
			sum += d;
		return sum / a.length;
	}

	
	public static double computeStdofArray(double[] a){
		
		double v = computeAverageValueofArray(a);
		
		double sum = 0;
		for(double x : a){
			sum += (x-v)*(x-v);
		}
		sum /= (a.length - 1);
		return Math.sqrt(sum);
	}
	
	/**
	 * compute Normalized cross-correlation
	 */
	public static double computeNCC(double[] a, double[] b){

		double av = computeAverageValueofArray(a);
		double bv = computeAverageValueofArray(b);

		double a_std = computeStdofArray(a);
		double b_std = computeStdofArray(b);
		
		double sum = 0;
		for(int i = 0 ; i < a.length; i++){
			
			double tmp = (a[i] - av) * (b[i] - bv);

			tmp /= a_std;
			tmp /= b_std;
			sum += tmp;
		}
		return sum / a.length;
		
		
//		double sum = 0;
//		double sqsum_a = 0, sqsum_b = 0;
//		for(int i = 0 ; i < a.length; i++){
//			sum += (a[i] - av) * (b[i]- bv);
//			sqsum_a += Math.pow(a[i] - av, 2);
//			sqsum_b += Math.pow(b[i] - bv, 2);
//		}
//		double re = sum / Math.sqrt(sqsum_a * sqsum_b);
//		
//		return re;
		
		
	}
	
	public static int indexOfArray(double[] a, double v ){

		for (int i = 0; i < a.length; i++) {
			
		      Double obj1 = new Double(v);
		      Double obj2 = new Double(a[i]);
			
			  if (obj1.equals(obj2)) 
				  return i;
			}
		return -1;
		
	}
	
	
	public static int kendallTauDistance(double[] a, double[] b) {
		int inversions = 0;

		for (int i = 0; i < a.length; i++) {
			
			for(int j = i + 1; j < a.length; j++){
				
				double v1 = a[i];
				double v2 = a[j];

				double i1 = indexOfArray(b, v1);
				double i2 = indexOfArray(b, v2);
				if( i2 < i1)
					inversions ++;
			}
		}
		return inversions;
	}

}

class ValueComparator implements Comparator {
	 
	  Map map;
	 
	  public ValueComparator(Map map){
	    this.map = map;
	  }
	  
	  public int compare(Object keyA, Object keyB){
	 
	    Comparable valueA = (Comparable) map.get(keyA);
	    Comparable valueB = (Comparable) map.get(keyB);
	 
	    if(valueA.equals(valueB))
	    	return 1;
	    return valueB.compareTo(valueA);
	 
	  }
}