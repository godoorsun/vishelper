package cn.edu.zjut.vis.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class VTimeHelper {

	
	public static int dateToTimeStamp(String dateStr, String dateFormat){
		
		//dateFormat yyyy/MM/dd HH:mm:ss
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat); 
		try {
			Date date = sdf.parse(dateStr);
			return (int) (date.getTime() / 1000);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	
	public static String timeStamptoDateStr(int timestamp, String dateFormat){
		
		long l_timestamp = timestamp * 1000L;
		//dateFormat yyyy-MM-dd HH:mm:ss
		Date date = new Date(l_timestamp);  
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat); 
		try {
			String s = sdf.format(date);
			return s;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}
	
	
	

	public static long startTime;
	
	public static void letsBeginToDoSomething() {
		startTime = System.currentTimeMillis();
	}

	public static int letsSeeHowlongItTakes(boolean isDisplayTime) {
		long endTime = System.currentTimeMillis();
		if (isDisplayTime)
			System.out.println((endTime - startTime) + "ms");
		return (int) (endTime - startTime);
	}
	
	public static int daysBetween(Calendar day1, Calendar day2){
		
		if(day1 == null || day2 == null)
			return -1;
		
	    if (day1.get(Calendar.YEAR) == day2.get(Calendar.YEAR)) {
	        return day2.get(Calendar.DAY_OF_YEAR) - day1.get(Calendar.DAY_OF_YEAR);
	    } else {	  
		    Calendar dayOne = (Calendar) day1.clone(),
		            dayTwo = (Calendar) day2.clone();
		    int diffDays  = 0 ;
			if(dayOne.before(dayTwo)){
				while (dayOne.before(dayTwo)) {
					dayOne.add(Calendar.DAY_OF_MONTH, 1);
				    diffDays++;
				}
			}
			else{		
				while (dayOne.after(dayTwo)) {
					dayOne.add(Calendar.DAY_OF_MONTH, -1);
				    diffDays--;
				}
			}
			return diffDays;
	    	
	    }
	        
	}

}
