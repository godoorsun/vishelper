package cn.edu.zjut.vis.database;

import java.util.Map;

import org.mapdb.DB;
import org.mapdb.DBMaker;

public class VDatabaseHelper {

	public static DB db = DBMaker
            .newDirectMemoryDB()
            .transactionDisable()
            .asyncWriteFlushDelay(100)
            .compressionEnable()
            .make();
	
	public static Map<Integer, Integer> getMapdb_int_int(String name){
		
		return db.getTreeMap(name);
	}
	
	
	public static Map<String, Integer> getMapdb_String_int(String name){
		
		return db.getTreeMap(name);
	}
}
