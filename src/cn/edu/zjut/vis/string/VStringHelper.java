package cn.edu.zjut.vis.string;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import cn.edu.zjut.vis.type.TwoTuple;

public class VStringHelper {

	
	
	public static final String TAG_PATTERN_String = "(?:^|\\s|[\\p{Punct}&&[^/]])(#[\\p{L}0-9-_]+)";
	public static final Pattern TAG_PATTERN = Pattern.compile(TAG_PATTERN_String);
	
	public static final String MENTION_PATTERN_String = "(?:^|\\s|[\\p{Punct}&&[^/]])(@[\\p{L}0-9-_]+)";
	public static final Pattern MENTION_PATTERN = Pattern.compile(MENTION_PATTERN_String);

	public static final String URL_PATTERN_String = "\\b(http?|ftp|file|https):(\\\\/\\\\/|//)[-a-zA-Z0-9+&@#/%?=~_|!:,.;\\\\]*[-a-zA-Z0-9+&@#/%=~_|\\\\]";
	public static final Pattern URL_PATTERN = Pattern.compile(URL_PATTERN_String);
	
	public static final String HTML_PATTERN_String = "&[a-zA-Z]+;";
	public static final Pattern HTML_PATTERN = Pattern.compile(HTML_PATTERN_String);
	
	public static SimilarityStrategy strategy = new JaroWinklerStrategy();
	public static StringSimilarityService service = new StringSimilarityServiceImpl(strategy);
	
	public static Set<String> Stopwords = null;

	public static StringUtils getStringUtils(){
		return new StringUtils();
	}
	
	
	public static boolean isAlphanumeric(String s){
		return StringUtils.isAlphanumeric(s);
		
	}
	
	public static boolean isNoAlphanumeric(String s){
		
		for(int i = 0; i < s.length(); i++){
			if(StringUtils.isAlphanumeric(s.subSequence(i, i+1)) == true){
				return false;
			}
		}
		return true;
	}
	
	public static boolean isAlphanumericSpace(String s){
		return StringUtils.isAlphanumericSpace(s);
	}
	
	
	
	//--------------------------------------------------------------------------------
	public static AhoCorasick StringMatching_AhoCorasick = new AhoCorasick();
	public static AhoCorasick StringMatching_AhoCorasick_Stopwords = new AhoCorasick();
	
	
	public static void buildAhoCorasick(ArrayList<String> keywords){
        List<byte[]> list = new ArrayList<byte[]>();
        for (int i = 0; i < keywords.size(); i++) {
            list.add(keywords.get(i).toLowerCase().getBytes());
        }
        StringMatching_AhoCorasick.build(list);
	}
	
	public static void buildAhoCorasick(AhoCorasick ahoCorasick, ArrayList<String> keywords){
        List<byte[]> list = new ArrayList<byte[]>();
        for (int i = 0; i < keywords.size(); i++) {
            list.add(keywords.get(i).toLowerCase().getBytes());
        }
        ahoCorasick.build(list);
	}
	
	//--------------------------------------------------------------------------------
	
	
	
	/**
	 * get Proportion Between KeywordsList And Text
	 * We need to count keyword, hashtags and url, then calculate the proportion..
	 * case-insensitive
	 * @param keywords
	 * @param text
	 * @return TwoTuple<Double, Integer> hit的string占据的比例，以及hit的count
	 */
	public static TwoTuple<Double, Integer> getProportionAndHitCountBetweenKeywordsListAndText(Set<String> Stopwords, String text){
		

		
		
		text = text.toLowerCase();
        int startIndex = 0;
        int count = 0;
        while(true){
        	TwoTuple<String, Integer> result = StringMatching_AhoCorasick.match(startIndex, text.getBytes());
        	
        	if (result != null && !"".equals(result.first)) {
        		//System.out.println(result.first);
        		startIndex = result.second + 1;
        		count ++;
        	}
        	else
        		break;
        }
        
        Matcher matcher = URL_PATTERN.matcher(text);
		while (matcher.find()){
			count++;
		}
        
        
        
        int allWordsCount = getMeaningfulWordCountInSpiltedText(text, Stopwords);
        
        
        
        
        return new TwoTuple<Double, Integer>(count * 1.0 / allWordsCount, count);
	}
	
	public static double getStringSimilarity(String text1, String text2){
		return service.score(text1, text2);
	}
	
	public static String[] getSpiltedWords(String text){
		return text.replace("", "").trim().split(" ");
	}
	
	public static int getMeaningfulWordCountInSpiltedText(String text, Set<String> stopwords){
		
		String words[] = getSpiltedWords(text);
        int allWordsCount = words.length;
        //count stop words
        for(String word : words){
        	if(word.length()== 1 && !StringUtils.isAlphanumeric(word))
        		allWordsCount --;
        	if(word.equals(""))
        		allWordsCount --;
        	if(stopwords.contains(word))
        		allWordsCount --;
        }
        if(allWordsCount < 0)
        	allWordsCount = 1;
        
        return allWordsCount;
	}
	
	
	public static void loadStopwords() {
		
		
		Stopwords = new HashSet<String>();
		String line;
		try {
			BufferedReader reader = new BufferedReader( new InputStreamReader(new FileInputStream("data/stopwords.txt"),"utf-8" ));
			while((line = reader.readLine()) != null){
				
				if(line.indexOf("#") != -1 || line.equals(""))
					continue;
				Stopwords.add(line);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	
	public  static boolean stopwordsCheck(String s){
		
		if(Stopwords == null)
			loadStopwords();
		return Stopwords.contains(s);
		
	}
	
	
	
}
