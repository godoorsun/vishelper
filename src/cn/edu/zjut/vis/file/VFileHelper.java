package cn.edu.zjut.vis.file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.sound.sampled.Line;

import com.twmacinta.util.MD5;

import cn.edu.zjut.vis.print.VPrintHelper;

//TODO log file needed.
public class VFileHelper { 
	
	
	public static BufferedReader getBufferedReader(String fileName) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), "utf-8"), 25600000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reader;
	}
	
	/**
	 * @param fileName
	 * @param bufferSize the unit is byte
	 * @return
	 */
	public static BufferedReader getBufferedReader(String fileName, int bufferSize) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), "utf-8"), bufferSize);  //bufferSize: byte
		} catch (Exception e) {
			e.printStackTrace();
		}
		return reader;
	}

	public static BufferedWriter getBufferedWriter(String fileName) {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(fileName), "utf-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return writer;
	}
	
	public static BufferedWriter getBufferedWriter(String fileName, boolean append) {
		BufferedWriter writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(fileName, append), "utf-8"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return writer;
	}
	
	/**
	 * 读取所有行放到list中
	 * @param fileName
	 * @return
	 */
	public static ArrayList<String> readLines(String fileName){
		ArrayList<String> lines = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), "utf-8"));
			String line;
			while( (line = reader.readLine()) != null ){
				lines.add(line);
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lines;
	}	
	
	public static ArrayList<String> readLines(String fileName, String encoding){
		ArrayList<String> lines = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), encoding));
			String line;
			while( (line = reader.readLine()) != null ){
				lines.add(line);
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lines;
	}
	
	
	public static ArrayList<String> readLines(String fileName, int lineAmount){
		int count  = 0;
		ArrayList<String> lines = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), "utf-8"));
			String line;
			while( (line = reader.readLine()) != null ){
				lines.add(line);
				count ++;
				if(count == lineAmount)
					return lines;
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lines;
	}
	
	public static ArrayList<String> readLines(String fileName, int lineAmount, String encoding){
		int count  = 0;
		ArrayList<String> lines = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), encoding));
			String line;
			while( (line = reader.readLine()) != null ){
				lines.add(line);
				count ++;
				if(count == lineAmount)
					return lines;
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lines;
	}
	
	
	
	public static HashMap<String, Integer> getKeyValueFromFile(String fileName, String spilt){
		HashMap<String, Integer> re = new HashMap<String, Integer>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), "utf-8"));
			String line;
			while( (line = reader.readLine()) != null ){
				re.put(line.split(spilt)[0], Integer.parseInt(line.split(spilt)[1]));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return re;
	}
	
	/**
	 * 读取某一列的所有行，放到list中
	 * @param fileName
	 * @param sep
	 * @param col
	 * @return
	 */
	public static ArrayList<String> readLinesByColumn(String fileName, String sep, int col){
		ArrayList<String> lines = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), "utf-8"));
			String line;
			while( (line = reader.readLine()) != null ){
				String term = line.split(sep)[col];
				lines.add(term);
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lines;
	}
	
	
	public static Set<String> readLinesIntoSet(String fileName){
		Set<String> lines = new HashSet<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), "utf-8"));
			String line;
			while( (line = reader.readLine()) != null ){
				lines.add(line);
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lines;
	}

	public static Set<String> readLinesIntoSet(String fileName, String ignoreString){
		Set<String> lines = new HashSet<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), "utf-8"));
			String line;
			while( (line = reader.readLine()) != null ){
				if(line.indexOf(ignoreString) != -1)
					continue;
				lines.add(line);
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lines;
	}
	
	public static ArrayList<String> readLinesIntoArrayList(String fileName, String ignoreString){
		ArrayList<String> lines = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), "utf-8"));
			String line;
			while( (line = reader.readLine()) != null ){
				if(line.indexOf(ignoreString) != -1)
					continue;
				lines.add(line);
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lines;
	}
	
	public static ArrayList<String> readLinesIntoArrayList(String fileName, String ignoreString, String encoding){
		ArrayList<String> lines = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					new FileInputStream(fileName), encoding));
			String line;
			while( (line = reader.readLine()) != null ){
				if(line.indexOf(ignoreString) != -1)
					continue;
				lines.add(line);
			}
			reader.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lines;
	}
	
	
	//--------------------------------------------------------------------------------
	public static ArrayList<String> files = new ArrayList<String>();
	public static ArrayList<String> getFilePathList(String folderPath){
		files.clear();
		iterateFilesandDirectory(new File(folderPath));
		return files;
	}
	
	public static ArrayList<String> getFilePathOnlyFileList(String folderPath){
		files.clear();
		File flist[] = new File(folderPath).listFiles();
		for (File f : flist) {
			if (f.isFile()) {
				// 这里将列出所有的文件夹
				files.add(f.getAbsolutePath());
			}
		}
		return files;
	}
	
	public static void iterateFilesandDirectory(File file) {
		
		if(file.isFile()){
			files.add(file.getAbsolutePath());
			return;
		}
		
		File flist[] = file.listFiles();
		if (flist == null || flist.length == 0) {
			return;
		}
		for (File f : flist) {
			if (f.isFile()) {
				// 这里将列出所有的文件夹
				files.add(f.getAbsolutePath());
			} else if(f.isDirectory()){
				// 这里将列出所有的文件
				// System.out.println(f.getAbsolutePath());
				iterateFilesandDirectory(f);
			}
		}
	}
	
	public static ArrayList<String> directories = new ArrayList<String>();
	public static ArrayList<String> getDirectories(String folderPath){
		directories.clear();
		iterateDirectory(new File(folderPath));
		return directories;
	}
	public static void iterateDirectory(File file) {
		
		File flist[] = file.listFiles();
		if (flist == null || flist.length == 0) {
			return;
		}
		for (File f : flist) {
			if (f.isFile()) {
				continue;
			} else if(f.isDirectory()){
				directories.add(f.getAbsolutePath());
			}
		}
	}
	
	
	public static int countLine(String file) throws IOException{
		BufferedReader br = VFileHelper.getBufferedReader(file, 10000000);
		String line = null;
		int count  = 0;
		while((line = br.readLine()) != null){
			count ++;
			if(count % 1000000 == 0)
				VPrintHelper.printlnInTabFormat(count);
		}
		br.close();
		return count;
	}
	
	
	/**
	 * [ ) rule 
	 * @param file
	 * @param line1  
	 * @param line2
	 * @throws IOException 
	 */
	public static void printLinesBetween(String file, int line1, int line2) {

		try{
			BufferedReader br = VFileHelper.getBufferedReader(file, 10000000);
			String line = null;
			int count  = 0;
			while((line = br.readLine()) != null){
				count ++;
				if(count >= line1 && count < line2)
					VPrintHelper.printlnInTabFormat(line);
				
				if(count > line2){
					break;
				}
				
			}
			br.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
 
	
	
	public static void main(String[] args) throws Exception{
		
		String hash = MD5.asHex(MD5.getHash(new File("/Users/sungodoor/Desktop/FetchedData/2015-12-03.txt")));
		
		System.out.println(hash);
		
	}
	
	
	
	
}
