package cn.edu.zjut.vis.print;

import java.io.BufferedWriter;
import java.util.Collection;
import java.util.Iterator;

import cn.edu.zjut.vis.file.VFileHelper;

public class VPrintHelper {

	public static boolean displayDebugInfo = true;
	
	
	
	private BufferedWriter bw = null;
	
	public VPrintHelper(String file){
		bw = VFileHelper.getBufferedWriter(file);
	}
	
	/**
	 * write the information to the file, the file is created in the construction method.
	 * @param arg
	 */
	public void ppf(Object... arg) {
		try{
			for (int i = 0; i < arg.length; i++)
				bw.write(arg[i] + "\t");
			bw.newLine();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	public static void printInTabFormat(Object... arg) {
		if (displayDebugInfo == false)
			return;
		for (int i = 0; i < arg.length; i++)
			System.out.print(arg[i] + "\t");
	}
	public static void printlnInTabFormat(Object... arg) {
		if (displayDebugInfo == false)
			return;
		for (int i = 0; i < arg.length; i++)
			System.out.print(arg[i] + "\t");
		System.out.println();
	}
	

	public static void pp(Object... arg) {
		if (displayDebugInfo == false)
			return;
		for (int i = 0; i < arg.length; i++)
			System.out.print(arg[i] + "\t");
		System.out.println();
	}
	
	public static void hprintInTabFormat(Object... arg) {
		if (displayDebugInfo == false)
			return;
		for (int i = 0; i < arg.length; i++)
			System.err.print(arg[i] + "\t");
	}
	
	public static void hprintlnInTabFormat(Object... arg) {
		if (displayDebugInfo == false)
			return;
		for (int i = 0; i < arg.length; i++)
			System.err.print(arg[i] + "\t");
		System.err.println();
	}
	
	public static void printCollection(Collection c, String sep){
	    for (Iterator iterator = c.iterator(); iterator.hasNext();) {
	        System.out.print(iterator.next() + sep);
	    }
	    System.out.println();
	}
}
