package cn.edu.zjut.vis.type;

public class TwoTuple<A, B> {
    public A first;
    public B second;
     

    public TwoTuple(A a, B b) {
        this.first = a;
        this.second = b;
    }
    
    public TwoTuple() {
        this.first = null;
        this.second = null;
    }

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return first.toString() + "\t" + second.toString();
	}


	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return first.hashCode() + second.hashCode();
	}
	

}

