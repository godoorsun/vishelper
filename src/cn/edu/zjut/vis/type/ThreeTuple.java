package cn.edu.zjut.vis.type;

import java.io.Serializable;

public class ThreeTuple<A, B, C> implements Serializable {
    public A first;
    public B second;
    public C third;
     

    public ThreeTuple(A a, B b, C c) {
        this.first = a;
        this.second = b;
        this.third = c;
    }
    
    public ThreeTuple() {
        this.first = null;
        this.second = null;
        this.third = null;
    }

	@Override
	public String toString() {
		return first.toString() + "\t" + second.toString() + "\t" + third.toString();
	}
    

}

