package test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.ss.usermodel.*;

import cn.edu.zjut.vis.collections.VCollectionsBiMap;
import cn.edu.zjut.vis.print.VPrintHelper;
import cn.edu.zjut.vis.string.JaroWinklerStrategy;
import cn.edu.zjut.vis.string.SimilarityStrategy;
import cn.edu.zjut.vis.string.StringSimilarityService;
import cn.edu.zjut.vis.string.StringSimilarityServiceImpl;

public class Test {

	
	public static SimilarityStrategy strategy = new JaroWinklerStrategy();
	public static StringSimilarityService service = new StringSimilarityServiceImpl(strategy);
	
	
	public static void  readExcel() throws Exception{
	    InputStream inp = new FileInputStream("/Users/sungodoor/Desktop/Microsoft.xlsm");
	    //InputStream inp = new FileInputStream("workbook.xlsx")
	    
	    int maxRow = 3001;
	    int startRow = 1;

	    Workbook wb = WorkbookFactory.create(inp);
	    Sheet sheet = wb.getSheetAt(0);
	    
	    for(int i = startRow; i < maxRow; i++){
	    	Row row = sheet.getRow(i);
		    Cell text = row.getCell(0);
		    Cell yesOrNo = row.getCell(1);
		    if (text == null || yesOrNo == null)
		    	continue;
		    int result = -1;
		    if(yesOrNo.getStringCellValue().equals("YES"))
		    	result = 1;
		    else
		    	result = 0;
		    VPrintHelper.printlnInTabFormat(result, text.getStringCellValue());
	    }

	    // Write the output to a file
	    //FileOutputStream fileOut = new FileOutputStream("workbook.xls");
	    //wb.write(fileOut);
	    //fileOut.close();
	}
	
	
	public static void regex(){
		

		String ss = "#__WindowsPhone__##8.1 aa_bb_cc will.. reportedly>> ?support??’ae%%up to 10-inch “screens,";
		
		
		String sp[] = ss.split("[\\s\\p{Punct}&&[^_\\-.]]+");
		for(String s : sp){
			if(s.equals(""))
				continue;
			VPrintHelper.printInTabFormat(s);
		}
		VPrintHelper.printlnInTabFormat();
		
		for(String s : sp){
			s = s.toLowerCase().trim();
			s = s.replaceAll("^[\\p{Punct}： 。 ；  ， ’： “ ”（ ） 、 ？ 《 》]*", "");  //remove trailing and heading characters.
			s = s.replaceAll("[\\p{Punct}： 。 ；  ， ’： “ ”（ ） 、 ？ 《 》]*$", "");
			if(s.equals(""))
				continue;
			VPrintHelper.printInTabFormat(s + "~");
		}
		VPrintHelper.printlnInTabFormat();
		
		Pattern p = Pattern.compile("[a-zA-Z0-9]+([.\\-_]*[a-zA-Z0-9])*");
		Matcher m  = p.matcher(ss);
		while(m.find())
			VPrintHelper.printInTabFormat(m.group() + "~");
		
		
		
		
		
	}
	
	public static void main(String[] args) throws Exception {
		
		//readExcel();
		
		//System.out.println("asdf");
		
		
		VCollectionsBiMap a = new VCollectionsBiMap();
		
		a.put("123", "bbb");
		System.out.println(a.getKey("bbb"));
		
		
		//regex();
		
		//String target = "Rose Decal Skin Sticker Cover Fit For Ipad2 Ipad3 Ipad4 $14.99 http://t.co/1ug0FYaBH9 #ipad #tablet";
		//String source = "Blue Kitty Decal Skin Sticker Cover Fit For Ipad2 Ipad3 Ipad4 $14.99 http://t.co/Mk8LyWmP4j #ipad #tablet";
		
			
		
		
//		ArrayList<String> list =  GuodaoToolbox.readLines("D:\\workspace\\DataPreprocessForVis2014\\techdata\\topic_and_their_tweets\\topic_and_their_tweets_topN_RandomN\\Apple.txt");
//		
//		for(int i = 0; i < list.size(); i++)
//			for(int j = i + 1; j < list.size(); j++){
//				double score = service.score(list.get(i), list.get(j)); // Score is 0.90
//				
//				if(score > 0.95){
//					GuodaoToolbox.printlnInTabFormat(i,j, score);
//					System.out.println(list.get(i));
//					System.out.println(list.get(j));
//				}
//					 
//				
//			}
				
		//GuodaoToolbox.printlnInTabFormat("          #TeamApple #Iphone5 #MacBook #Ipad".split(" ").length);
				
		//double score = service.score(source, target); // Score is 0.90
		//GuodaoToolbox.printlnInTabFormat(score);
		
	}

}
